provider "google" {
  project = "searce-playground"
}

# create vpc 

resource "google_compute_network" "net1" {
  name = "mayank-network"
}

# create public subnet in us-east1 region

resource "google_compute_subnetwork" "subnet3" {
  name          = "mayank-publicsubnetwork"
  network       = google_compute_network.net1.id
  ip_cidr_range = "10.0.1.0/24"
  region        = "us-east1"
}

# create instance template for MIG in us-east1

resource "google_compute_instance_template" "default" {
  name        = "mayankterraform-template"
  description = "This template is used to create app server instances."

  depends_on = [
    google_compute_subnetwork.subnet3
  ]

  labels = {
    owner = "mayank"
  }
  tags = ["wordpress"]

  metadata_startup_script = <<-EOF
  #! /bin/bash
  sudo apt install apache2 php php-mysql -y
  sudo wget -P /var/www/html https://wordpress.org/latest.tar.gz
  sudo tar -xvf /var/www/html/latest.tar.gz -C  /var/www/html
  sudo mv /var/www/html/wordpress/* /var/www/html/
  EOF

  instance_description = "description assigned to instances"
  machine_type         = "e2-small"

  scheduling {
    preemptible       = true
    automatic_restart = false
  }

  disk {
    source_image = "ubuntu-os-cloud/ubuntu-1804-lts"
  }

  network_interface {
    network    = google_compute_network.net1.id
    subnetwork = google_compute_subnetwork.subnet3.id
    access_config {

    }
  }
}

# create http health check for MIG

resource "google_compute_health_check" "autohealing" {
  name                = "autohealing-health-check"
  check_interval_sec  = 5
  timeout_sec         = 5
  healthy_threshold   = 2
  unhealthy_threshold = 10 # 50 seconds

  http_health_check {
    port = "80"
  }
}

# create instance group in us-east1-b region

resource "google_compute_instance_group_manager" "appserver" {
  name = "mayankgroup-terraform"

  base_instance_name = "mayankig"
  zone               = "us-east1-b"

  version {
    instance_template = google_compute_instance_template.default.id
  }


  target_size = 1



  auto_healing_policies {
    health_check      = google_compute_health_check.autohealing.id
    initial_delay_sec = 300
  }
}

# create firewall rule to allow http health check and ssh 

resource "google_compute_firewall" "fw1" {
  name    = "test1-firewall"
  network = google_compute_network.net1.name

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = ["130.211.0.0/22", "35.191.0.0/16", "171.79.119.145/32"]
  target_tags   = ["wordpress"]
}

resource "google_compute_firewall" "fw2" {
  name    = "test-firewall"
  network = google_compute_network.net1.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["35.235.240.0/20"]
  target_tags   = ["wordpress"]
}

# create external ip for laod balancer

resource "google_compute_global_address" "paas-monitor" {
  name = "mayank-global-ip"
}

# create forwarding rule for http external lb

resource "google_compute_global_forwarding_rule" "paas-monitor1" {
  name       = "mayankforwarding-port-80"
  ip_address = google_compute_global_address.paas-monitor.address
  port_range = "80"
  target     = google_compute_target_http_proxy.paas-monitor2.self_link
}

# create http target proxy 

resource "google_compute_target_http_proxy" "paas-monitor2" {
  name    = "mayankhttp-proxy"
  url_map = google_compute_url_map.paas-monitor4.self_link

}

# create url map 

resource "google_compute_url_map" "paas-monitor4" {
  name            = "mayankurl-map"
  default_service = google_compute_backend_service.paas-monitor3.self_link
  # path_matcher {
  #   default_service = "value"
  #   path_rule {
  #     paths = [ "" ]
  #     service = "value"
  #   }
  # }
}

# create backend for lb

resource "google_compute_backend_service" "paas-monitor3" {
  name             = "mayank-backend"
  protocol         = "HTTP"
  port_name        = "mayank-port"
  timeout_sec      = 10
  session_affinity = "NONE"


  backend {
    group = google_compute_instance_group_manager.appserver.instance_group
  }
  health_checks = [google_compute_health_check.autohealing.id]
}

# create internal static ip for cloud sql 

resource "google_compute_global_address" "private_ip_address" {

  name          = "mayank-private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.net1.id
}

# create private service connection 

resource "google_service_networking_connection" "private_vpc_connection" {

  network                 = google_compute_network.net1.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

resource "random_id" "db_name_suffix" {
  byte_length = 4
}

# create cloud sql database 

resource "google_sql_database_instance" "instance" {

  name   = "mayank-private-instance-${random_id.db_name_suffix.hex}"
  region = "us-east1"

  depends_on          = [google_service_networking_connection.private_vpc_connection]
  deletion_protection = false
  database_version    = "MYSQL_5_7"
  root_password       = "redhat"

  settings {
    tier = "db-f1-micro"

    ip_configuration {
      ipv4_enabled    = false
      private_network = google_compute_network.net1.id
    }

    user_labels = {
      "owner" = "mayank"
    }
  }
}

# create cloud sql database

resource "google_sql_database" "database" {
  name     = "mydb"
  instance = google_sql_database_instance.instance.name
  depends_on = [
    google_sql_database_instance.instance
  ]
}

# create cloud sql user for cloud sql database  

resource "google_sql_user" "users" {
  name     = "mayank"
  instance = google_sql_database_instance.instance.name
  password = "mayank"
  depends_on = [
    google_sql_database_instance.instance
  ]
}

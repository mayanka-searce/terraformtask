provider "google" {
  project = "searce-playground"
}

# create new vpc

resource "google_compute_network" "net" {
  name = "mayank-network"
}

# create two subnet i.e private and public subnet in us-east1 region

resource "google_compute_subnetwork" "subnet" {
  name          = "mayank-privatesubnetwork"
  network       = google_compute_network.net.id
  ip_cidr_range = "10.0.2.0/24"
  region        = "us-east1"
}
resource "google_compute_subnetwork" "subnet2" {
  name          = "mayank-publicsubnetwork"
  network       = google_compute_network.net.id
  ip_cidr_range = "10.0.1.0/24"
  region        = "us-east1"
}

# create router for private subnet

resource "google_compute_router" "router" {
  name    = "mayank-router"
  region  = google_compute_subnetwork.subnet.region
  network = google_compute_network.net.id
}

# create external ip for cloud NAT

resource "google_compute_address" "address" {
  count  = 2
  name   = "nat-manual-ip-${count.index}"
  region = google_compute_subnetwork.subnet.region
}

# create cloud NAT for private subnet

resource "google_compute_router_nat" "nat_manual" {
  name   = "mayank-router-nat"
  router = google_compute_router.router.name
  region = google_compute_router.router.region

  nat_ip_allocate_option = "MANUAL_ONLY"
  nat_ips                = google_compute_address.address.*.self_link

  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
  subnetwork {
    name                    = google_compute_subnetwork.subnet.id
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}

# allow ssh on database

resource "google_compute_firewall" "fw2" {
  name    = "test-firewall"
  network = google_compute_network.net.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["35.235.240.0/20"]
  target_tags   = ["database"]
}

# allow http on wordpress instance

resource "google_compute_firewall" "fw1" {
  name    = "test1-firewall"
  network = google_compute_network.net.name

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = ["171.79.119.145/32"]
  target_tags   = ["wordpress"]
}

# create database in private subnet 

resource "google_compute_instance" "default" {
  name         = "mayank-database"
  machine_type = "e2-small"
  zone         = "us-east1-b"
  labels = {
    "owner" = "mayank"
  }
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }
  network_interface {
    network    = google_compute_network.net.name
    subnetwork = google_compute_subnetwork.subnet.name
  }
  tags = ["database"]

  scheduling {
    preemptible       = true
    automatic_restart = false
  }

}

# create wordpress instance in public subnet

resource "google_compute_instance" "wordpress" {
  name         = "mayank-wordpress"
  machine_type = "e2-small"
  zone         = "us-east1-b"
  labels = {
    "owner" = "mayank"
  }

  metadata_startup_script = <<-EOF
  #!/bin/bash;
  sudo apt install apache2 php php-mysql -y;
  sudo wget -P /var/www/html https://wordpress.org/latest.tar.gz;
  sudo tar -xvf /var/www/html/latest.tar.gz -C  /var/www/html
  sudo mv /var/www/html/wordpress/* /var/www/html/"
  EOF

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  network_interface {
    network    = google_compute_network.net.name
    subnetwork = google_compute_subnetwork.subnet2.name
  }

  service_account {
    email  = "531268413988-compute@developer.gserviceaccount.com"
    scopes = ["storage-full"]
  }

  tags = ["wordpress"]

  scheduling {
    preemptible       = true
    automatic_restart = false
  }

}

# create google cloud storage 

resource "google_storage_bucket" "static-site" {
  name          = "mayanktestbucketterraform"
  location      = "US"
  force_destroy = true
  storage_class = "STANDARD"
  uniform_bucket_level_access = true
}
